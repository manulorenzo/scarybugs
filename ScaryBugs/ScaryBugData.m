//
//  ScaryBugData.m
//  ScaryBugs
//
//  Created by manu on 02/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "ScaryBugData.h"

@implementation ScaryBugData
@synthesize title = _title;
@synthesize rating = _rating;

- (id)initWithTitle:(NSString *)title rating:(float)rating {
    if (self = [super init]) {
        self.title = title;
        self.rating = rating;
    }
return self;
}

@end
