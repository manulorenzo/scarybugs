//
//  MasterViewController.h
//  ScaryBugs
//
//  Created by manu on 02/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController
@property (strong) NSMutableArray *bugs;
@end
