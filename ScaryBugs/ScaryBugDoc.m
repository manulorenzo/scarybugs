//
//  ScaryBugDoc.m
//  ScaryBugs
//
//  Created by manu on 02/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "ScaryBugDoc.h"
#import "ScaryBugData.h"

@implementation ScaryBugDoc
@synthesize data = _data;
@synthesize fullImage = _fullImage;
@synthesize thumbImage = _thumbImage;

- (id)initWithTitle:(NSString *)title rating:(float)rating thumbImage:(UIImage *)thumbImage fullImage:(UIImage *)fullImage {
    if (self = [super init]) {
        self.data = [[ScaryBugData alloc] initWithTitle:title rating:rating];
        self.thumbImage = thumbImage;
        self.fullImage  = fullImage;
    }
    return self;
}
@end